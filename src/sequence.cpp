/*
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
*/

#include "sequence.h"

#include <iostream>
#include <functional>
#include <numeric>
#include <optional>
#include <sstream>
#include <vector>

namespace fasta
{
  Sequence::Sequence(std::string header, std::string sequence, const char headerdelim)
    : seq(sequence), headerdelim(headerdelim)
    {
      tokenize(header);
    }

  Sequence::~Sequence()
    {}

  const std::string& Sequence::sequence() const
    {return seq;}

  const std::string Sequence::header(const std::optional<std::string>& delimiter) const
    {
      const std::string& delim = delimiter.value_or(std::string {headerdelim});
      return std::accumulate(std::next(headertokens.begin()),
                             headertokens.end(),
                             headertokens.front(),
                             [&delim](const std::string& a, const std::string b)
                                     {return a + delim + b;});
    }

  const std::int_fast32_t Sequence::length() const
    {return seq.length();}

  void Sequence::tokenize(const std::string& line)
  {
    std::string value;
    std::istringstream valuestream(line);
    while(std::getline(valuestream, value, headerdelim))
    {
      headertokens.push_back(value);
    }
  }

  const std::optional<std::string> Sequence::headertoken(long unsigned int idx) const
  {
    return(idx > headertokens.size()) ?  std::nullopt : std::optional<std::string>{headertokens[idx]};
  }

  //  blatantly assuming accession is first space demilited header value
  const std::string Sequence::accession() const
    {return headertoken(0).value_or("");}
} // namespace fasta
