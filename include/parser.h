/*
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 */

#pragma once

#include <iostream>
#include <unordered_map>

#include "processor.h"
#include "sequence.h"

namespace fasta
{
  class Parser
  {
    public:
      Parser();
      Parser(bool doStore, bool doProcess);
      ~Parser();
      void parse(fasta::Processor& proc);

    private:
      int parse_status = 0;
      int seqcount = 0;
      int linecount = 0;
      bool doStore =  false;
      bool doProcess = true;
      std::string header;
      std::string seq;
      std::unordered_map<std::string, Sequence> sequences;
      void reset();
      void add_sequence(fasta::Sequence, fasta::Processor& proc);
  };
} // end fasta namespace
