/*
 * -------------------------------------------------------------------------------
 * \author Jan Piotr Buchmann <jpb@members.fsf.org>
 * \copyright 2021
 * -------------------------------------------------------------------------------
 */

#include "fastaformatter.h"

#include <iostream>

#include "../include/fasta.h"


FastaFormatter::FastaFormatter()
{}
FastaFormatter::~FastaFormatter()
{}
void FastaFormatter::process(fasta::Sequence& seq)
{
  std::cout << ">" << seq.header() << "\n" << seq.sequence() << "\n";
}
